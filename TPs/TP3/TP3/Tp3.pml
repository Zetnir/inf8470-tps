

mtype:auth = { WrongUsername, ValidUsername, WrongPassword, ValidPassword, Blocked, Logout, LoggedOut, Login, LoggedIn};
mtype:operations = { AddFunds, FundsAdded, WithdrawFunds, FundsWithdrew, NoMoreFunds, NotEnoughFunds, BoughtMeal}

chan authIn = [2] of {mtype:auth};
chan authOut = [2] of {mtype:auth}

chan opIn = [4] of {mtype:operations};
chan opOut = [4] of {mtype:operations};

chan opDataIn = [4] of {mtype:operations};
chan opDataOut = [4] of {mtype:operations};

int funds = 0;
int count = 0;

bool accountLocked = false;
bool canBuyMeal = false;
bool clientConnected = false;
bool canAddFunds = true;
bool fundsWithdrew = false;

ltl p1 { []( <>(!accountLocked && ((count > 3) || (count == 3)))) };
ltl p2 { []((funds > 0) || (funds == 0)) };
ltl p3 { !canBuyMeal until clientConnected };
ltl p4 { [](<>fundsWithdrew) };
ltl p5 { [](<>canAddFunds)};

proctype Client(){
    authIn!Logout;
    printf("System \n");    
    do
    // ------ Authentification process ------
    :: authOut??LoggedOut ->    
        authentification:
        clientConnected = false;
        count = 0;
        canBuyMeal = false;
        canAddFunds = false;
        accountLocked = false;
        printf("LoogedOut \n");    
        if
        :: authIn!WrongUsername;
        :: authIn!ValidUsername;
        fi
        do
        :: authOut??WrongUsername;
            printf("WrongUsername \n");
            if
            :: authIn!WrongUsername;
            :: authIn!ValidUsername;
            fi
        :: authOut??ValidUsername;
            printf("ValidUsername \n");
            if
            :: authIn!WrongPassword;
            :: authIn!ValidPassword;
            fi
            do
            :: authOut??WrongPassword ->
                printf("WrongPassword \n");
                if
                :: authIn!WrongPassword;
                :: authIn!ValidPassword;
                fi
            :: authOut??Blocked ->
                printf("Account Blocked \n");
                goto authentification;
            :: authOut??ValidPassword ->authIn!Login;
                printf("ValidPassword \n");
                break;
            od
            break;
        od
    // ------ Operations process ------
    :: authOut??LoggedIn ->
        clientConnected = true;
        fundsWithdrew = false;
        canBuyMeal = true;
        canAddFunds = true;
    clientOperations :
        if
        :: opIn!AddFunds;
        :: opIn!WithdrawFunds;
        :: opIn!BoughtMeal;
        :: authIn!Logout;
        fi

        do
        :: opOut??FundsAdded ->printf("FundsAdded \n");
            goto clientOperations;
        :: opOut??FundsWithdrew ->printf("FundsWithdrew \n");
            fundsWithdrew = true;
            goto clientOperations;
        :: opOut??BoughtMeal ->printf("BoughtMeal \n");
            goto clientOperations;
        :: opOut??NoMoreFunds ->printf("NoMoreFunds \n");
            goto clientOperations;
        :: opOut??NotEnoughFunds ->printf("NotEnoughFunds \n");
            goto clientOperations;
        :: authOut??LoggedOut ->goto authentification;
        od
    od
    printf("End System \n");    
};

proctype Server(){
    do
    // ------ Authentification process ------
    :: authIn??Logout ->authOut!LoggedOut
        authServer:
        do
        :: authIn??WrongUsername ->authOut!WrongUsername;
        :: authIn??ValidUsername ->authOut!ValidUsername;
        :: authIn??WrongPassword ->
            count = count + 1;
            if
            :: count >= 3 ->authOut!Blocked;
                accountLocked = true;
            :: else ->authOut!WrongPassword;
            fi
        :: authIn??ValidPassword ->authOut!ValidPassword;
            break;
        od
    :: authIn??Login -> authOut!LoggedIn
        count = 0;
        do
        :: opIn??AddFunds ->opDataIn!AddFunds;
        :: opIn??WithdrawFunds ->
            if
            :: funds > 0 ->opDataIn!WithdrawFunds;
            :: else ->opOut!NoMoreFunds;
            fi
        :: opIn??BoughtMeal ->
            if
            :: funds >= 10 ->
                opDataIn!BoughtMeal;
            :: else -> opOut!NotEnoughFunds
            fi
        :: opDataOut??FundsAdded ->opOut!FundsAdded;
        :: opDataOut??FundsWithdrew ->opOut!FundsWithdrew;
        :: opDataOut??BoughtMeal -> opOut!BoughtMeal;
        :: authIn??Logout ->authOut!LoggedOut;
            goto authServer;
        od
    od
};

proctype Database(){
    do
    :: opDataIn??AddFunds ->funds = funds + 5;
        printf("%d", funds);
        opDataOut!FundsAdded;
    :: opDataIn??WithdrawFunds ->funds = 0;
        opDataOut!FundsWithdrew;
    :: opDataIn??BoughtMeal ->funds = funds - 10;
        printf("%d", funds);
        opDataOut!BoughtMeal;
    od
};

init {
    atomic {
        run Client()
        run Server();
        run Database();
    }
}