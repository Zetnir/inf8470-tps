#define rand	pan_rand
#define pthread_equal(a,b)	((a)==(b))
#if defined(HAS_CODE) && defined(VERBOSE)
	#ifdef BFS_PAR
		bfs_printf("Pr: %d Tr: %d\n", II, t->forw);
	#else
		cpu_printf("Pr: %d Tr: %d\n", II, t->forw);
	#endif
#endif
	switch (t->forw) {
	default: Uerror("bad forward move");
	case 0:	/* if without executable clauses */
		continue;
	case 1: /* generic 'goto' or 'skip' */
		IfNotBlocked
		_m = 3; goto P999;
	case 2: /* generic 'else' */
		IfNotBlocked
		if (trpt->o_pm&1) continue;
		_m = 3; goto P999;

		 /* CLAIM p5 */
	case 3: // STATE 1 - _spin_nvr.tmp:44 - [(!(canAddFunds))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[8][1] = 1;
		if (!( !(((int)now.canAddFunds))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 4: // STATE 8 - _spin_nvr.tmp:49 - [(!(canAddFunds))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported8 = 0;
			if (verbose && !reported8)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported8 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported8 = 0;
			if (verbose && !reported8)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported8 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[8][8] = 1;
		if (!( !(((int)now.canAddFunds))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 5: // STATE 13 - _spin_nvr.tmp:51 - [-end-] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported13 = 0;
			if (verbose && !reported13)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported13 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported13 = 0;
			if (verbose && !reported13)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported13 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[8][13] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* CLAIM p4 */
	case 6: // STATE 1 - _spin_nvr.tmp:33 - [(!(fundsWithdrew))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[7][1] = 1;
		if (!( !(((int)now.fundsWithdrew))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 7: // STATE 8 - _spin_nvr.tmp:38 - [(!(fundsWithdrew))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported8 = 0;
			if (verbose && !reported8)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported8 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported8 = 0;
			if (verbose && !reported8)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported8 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[7][8] = 1;
		if (!( !(((int)now.fundsWithdrew))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 8: // STATE 13 - _spin_nvr.tmp:40 - [-end-] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported13 = 0;
			if (verbose && !reported13)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported13 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported13 = 0;
			if (verbose && !reported13)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported13 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[7][13] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* CLAIM p3 */
	case 9: // STATE 1 - _spin_nvr.tmp:24 - [(!(clientConnected))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[6][1] = 1;
		if (!( !(((int)now.clientConnected))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 10: // STATE 3 - _spin_nvr.tmp:25 - [((!(!(canBuyMeal))&&!(clientConnected)))] (6:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported3 = 0;
			if (verbose && !reported3)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported3 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported3 = 0;
			if (verbose && !reported3)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported3 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[6][3] = 1;
		if (!(( !( !(((int)now.canBuyMeal)))&& !(((int)now.clientConnected)))))
			continue;
		/* merge: assert(!((!(!(canBuyMeal))&&!(clientConnected))))(0, 4, 6) */
		reached[6][4] = 1;
		spin_assert( !(( !( !(((int)now.canBuyMeal)))&& !(((int)now.clientConnected)))), " !(( !( !(canBuyMeal))&& !(clientConnected)))", II, tt, t);
		/* merge: .(goto)(0, 7, 6) */
		reached[6][7] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 11: // STATE 10 - _spin_nvr.tmp:29 - [-end-] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[6][10] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* CLAIM p2 */
	case 12: // STATE 1 - _spin_nvr.tmp:14 - [(!(((funds>0)||(funds==0))))] (6:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[5][1] = 1;
		if (!( !(((now.funds>0)||(now.funds==0)))))
			continue;
		/* merge: assert(!(!(((funds>0)||(funds==0)))))(0, 2, 6) */
		reached[5][2] = 1;
		spin_assert( !( !(((now.funds>0)||(now.funds==0)))), " !( !(((funds>0)||(funds==0))))", II, tt, t);
		/* merge: .(goto)(0, 7, 6) */
		reached[5][7] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 13: // STATE 10 - _spin_nvr.tmp:19 - [-end-] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[5][10] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* CLAIM p1 */
	case 14: // STATE 1 - _spin_nvr.tmp:3 - [(!((!(accountLocked)&&((count>3)||(count==3)))))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[4][1] = 1;
		if (!( !(( !(((int)now.accountLocked))&&((now.count>3)||(now.count==3))))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 15: // STATE 8 - _spin_nvr.tmp:8 - [(!((!(accountLocked)&&((count>3)||(count==3)))))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported8 = 0;
			if (verbose && !reported8)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported8 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported8 = 0;
			if (verbose && !reported8)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported8 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[4][8] = 1;
		if (!( !(( !(((int)now.accountLocked))&&((now.count>3)||(now.count==3))))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 16: // STATE 13 - _spin_nvr.tmp:10 - [-end-] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported13 = 0;
			if (verbose && !reported13)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported13 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported13 = 0;
			if (verbose && !reported13)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported13 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[4][13] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC :init: */
	case 17: // STATE 1 - Tp3.pml:165 - [(run Client())] (0:0:0 - 1)
		IfNotBlocked
		reached[3][1] = 1;
		if (!(addproc(II, 1, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 18: // STATE 2 - Tp3.pml:166 - [(run Server())] (0:0:0 - 1)
		IfNotBlocked
		reached[3][2] = 1;
		if (!(addproc(II, 1, 1)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 19: // STATE 3 - Tp3.pml:167 - [(run Database())] (0:0:0 - 1)
		IfNotBlocked
		reached[3][3] = 1;
		if (!(addproc(II, 1, 2)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 20: // STATE 5 - Tp3.pml:169 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[3][5] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC Database */
	case 21: // STATE 1 - Tp3.pml:152 - [opDataIn??7] (0:0:1 - 1)
		reached[2][1] = 1;
		if (q_len(now.opDataIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opDataIn, 1, 7))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opDataIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opDataIn);
		sprintf(simtmp, "%d", 7); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 22: // STATE 2 - Tp3.pml:152 - [funds = (funds+5)] (0:0:1 - 1)
		IfNotBlocked
		reached[2][2] = 1;
		(trpt+1)->bup.oval = now.funds;
		now.funds = (now.funds+5);
#ifdef VAR_RANGES
		logval("funds", now.funds);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 23: // STATE 3 - Tp3.pml:153 - [printf('%d',funds)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][3] = 1;
		Printf("%d", now.funds);
		_m = 3; goto P999; /* 0 */
	case 24: // STATE 4 - Tp3.pml:154 - [opDataOut!6] (0:0:0 - 1)
		IfNotBlocked
		reached[2][4] = 1;
		if (q_full(now.opDataOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opDataOut);
		sprintf(simtmp, "%d", 6); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opDataOut, 0, 6, 1);
		_m = 2; goto P999; /* 0 */
	case 25: // STATE 5 - Tp3.pml:155 - [opDataIn??5] (0:0:1 - 1)
		reached[2][5] = 1;
		if (q_len(now.opDataIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opDataIn, 1, 5))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opDataIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opDataIn);
		sprintf(simtmp, "%d", 5); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 26: // STATE 6 - Tp3.pml:155 - [funds = 0] (0:0:1 - 1)
		IfNotBlocked
		reached[2][6] = 1;
		(trpt+1)->bup.oval = now.funds;
		now.funds = 0;
#ifdef VAR_RANGES
		logval("funds", now.funds);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 27: // STATE 7 - Tp3.pml:156 - [opDataOut!4] (0:0:0 - 1)
		IfNotBlocked
		reached[2][7] = 1;
		if (q_full(now.opDataOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opDataOut);
		sprintf(simtmp, "%d", 4); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opDataOut, 0, 4, 1);
		_m = 2; goto P999; /* 0 */
	case 28: // STATE 8 - Tp3.pml:157 - [opDataIn??1] (0:0:1 - 1)
		reached[2][8] = 1;
		if (q_len(now.opDataIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opDataIn, 1, 1))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opDataIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opDataIn);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 29: // STATE 9 - Tp3.pml:157 - [funds = (funds-10)] (0:0:1 - 1)
		IfNotBlocked
		reached[2][9] = 1;
		(trpt+1)->bup.oval = now.funds;
		now.funds = (now.funds-10);
#ifdef VAR_RANGES
		logval("funds", now.funds);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 30: // STATE 10 - Tp3.pml:158 - [printf('%d',funds)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][10] = 1;
		Printf("%d", now.funds);
		_m = 3; goto P999; /* 0 */
	case 31: // STATE 11 - Tp3.pml:159 - [opDataOut!1] (0:0:0 - 1)
		IfNotBlocked
		reached[2][11] = 1;
		if (q_full(now.opDataOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opDataOut);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opDataOut, 0, 1, 1);
		_m = 2; goto P999; /* 0 */
	case 32: // STATE 15 - Tp3.pml:161 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[2][15] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC Server */
	case 33: // STATE 1 - Tp3.pml:111 - [authIn??4] (0:0:1 - 1)
		reached[1][1] = 1;
		if (q_len(now.authIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authIn, 1, 4))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authIn);
		sprintf(simtmp, "%d", 4); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 34: // STATE 2 - Tp3.pml:111 - [authOut!3] (0:0:0 - 1)
		IfNotBlocked
		reached[1][2] = 1;
		if (q_full(now.authOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authOut);
		sprintf(simtmp, "%d", 3); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authOut, 0, 3, 1);
		_m = 2; goto P999; /* 0 */
	case 35: // STATE 3 - Tp3.pml:114 - [authIn??9] (0:0:1 - 1)
		reached[1][3] = 1;
		if (q_len(now.authIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authIn, 1, 9))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authIn);
		sprintf(simtmp, "%d", 9); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 36: // STATE 4 - Tp3.pml:114 - [authOut!9] (0:0:0 - 1)
		IfNotBlocked
		reached[1][4] = 1;
		if (q_full(now.authOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authOut);
		sprintf(simtmp, "%d", 9); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authOut, 0, 9, 1);
		_m = 2; goto P999; /* 0 */
	case 37: // STATE 5 - Tp3.pml:115 - [authIn??8] (0:0:1 - 1)
		reached[1][5] = 1;
		if (q_len(now.authIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authIn, 1, 8))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authIn);
		sprintf(simtmp, "%d", 8); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 38: // STATE 6 - Tp3.pml:115 - [authOut!8] (0:0:0 - 1)
		IfNotBlocked
		reached[1][6] = 1;
		if (q_full(now.authOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authOut);
		sprintf(simtmp, "%d", 8); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authOut, 0, 8, 1);
		_m = 2; goto P999; /* 0 */
	case 39: // STATE 7 - Tp3.pml:116 - [authIn??7] (0:0:1 - 1)
		reached[1][7] = 1;
		if (q_len(now.authIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authIn, 1, 7))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authIn);
		sprintf(simtmp, "%d", 7); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 40: // STATE 8 - Tp3.pml:117 - [count = (count+1)] (0:0:1 - 1)
		IfNotBlocked
		reached[1][8] = 1;
		(trpt+1)->bup.oval = now.count;
		now.count = (now.count+1);
#ifdef VAR_RANGES
		logval("count", now.count);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 41: // STATE 9 - Tp3.pml:119 - [((count>=3))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][9] = 1;
		if (!((now.count>=3)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 42: // STATE 10 - Tp3.pml:119 - [authOut!5] (0:0:0 - 1)
		IfNotBlocked
		reached[1][10] = 1;
		if (q_full(now.authOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authOut);
		sprintf(simtmp, "%d", 5); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authOut, 0, 5, 1);
		_m = 2; goto P999; /* 0 */
	case 43: // STATE 11 - Tp3.pml:120 - [accountLocked = 1] (0:0:1 - 1)
		IfNotBlocked
		reached[1][11] = 1;
		(trpt+1)->bup.oval = ((int)now.accountLocked);
		now.accountLocked = 1;
#ifdef VAR_RANGES
		logval("accountLocked", ((int)now.accountLocked));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 44: // STATE 13 - Tp3.pml:121 - [authOut!7] (0:0:0 - 1)
		IfNotBlocked
		reached[1][13] = 1;
		if (q_full(now.authOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authOut);
		sprintf(simtmp, "%d", 7); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authOut, 0, 7, 1);
		_m = 2; goto P999; /* 0 */
	case 45: // STATE 16 - Tp3.pml:123 - [authIn??6] (0:0:1 - 1)
		reached[1][16] = 1;
		if (q_len(now.authIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authIn, 1, 6))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authIn);
		sprintf(simtmp, "%d", 6); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 46: // STATE 17 - Tp3.pml:123 - [authOut!6] (0:0:0 - 1)
		IfNotBlocked
		reached[1][17] = 1;
		if (q_full(now.authOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authOut);
		sprintf(simtmp, "%d", 6); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authOut, 0, 6, 1);
		_m = 2; goto P999; /* 0 */
	case 47: // STATE 22 - Tp3.pml:126 - [authIn??2] (0:0:1 - 1)
		reached[1][22] = 1;
		if (q_len(now.authIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authIn, 1, 2))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authIn);
		sprintf(simtmp, "%d", 2); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 48: // STATE 23 - Tp3.pml:126 - [authOut!1] (0:0:0 - 1)
		IfNotBlocked
		reached[1][23] = 1;
		if (q_full(now.authOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authOut);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authOut, 0, 1, 1);
		_m = 2; goto P999; /* 0 */
	case 49: // STATE 24 - Tp3.pml:127 - [count = 0] (0:0:1 - 1)
		IfNotBlocked
		reached[1][24] = 1;
		(trpt+1)->bup.oval = now.count;
		now.count = 0;
#ifdef VAR_RANGES
		logval("count", now.count);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 50: // STATE 25 - Tp3.pml:129 - [opIn??7] (0:0:1 - 1)
		reached[1][25] = 1;
		if (q_len(now.opIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opIn, 1, 7))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opIn);
		sprintf(simtmp, "%d", 7); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 51: // STATE 26 - Tp3.pml:129 - [opDataIn!7] (0:0:0 - 1)
		IfNotBlocked
		reached[1][26] = 1;
		if (q_full(now.opDataIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opDataIn);
		sprintf(simtmp, "%d", 7); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opDataIn, 0, 7, 1);
		_m = 2; goto P999; /* 0 */
	case 52: // STATE 27 - Tp3.pml:130 - [opIn??5] (0:0:1 - 1)
		reached[1][27] = 1;
		if (q_len(now.opIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opIn, 1, 5))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opIn);
		sprintf(simtmp, "%d", 5); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 53: // STATE 28 - Tp3.pml:132 - [((funds>0))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][28] = 1;
		if (!((now.funds>0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 54: // STATE 29 - Tp3.pml:132 - [opDataIn!5] (0:0:0 - 1)
		IfNotBlocked
		reached[1][29] = 1;
		if (q_full(now.opDataIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opDataIn);
		sprintf(simtmp, "%d", 5); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opDataIn, 0, 5, 1);
		_m = 2; goto P999; /* 0 */
	case 55: // STATE 31 - Tp3.pml:133 - [opOut!3] (0:0:0 - 1)
		IfNotBlocked
		reached[1][31] = 1;
		if (q_full(now.opOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opOut);
		sprintf(simtmp, "%d", 3); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opOut, 0, 3, 1);
		_m = 2; goto P999; /* 0 */
	case 56: // STATE 34 - Tp3.pml:135 - [opIn??1] (0:0:1 - 1)
		reached[1][34] = 1;
		if (q_len(now.opIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opIn, 1, 1))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opIn);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 57: // STATE 35 - Tp3.pml:137 - [((funds>=10))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][35] = 1;
		if (!((now.funds>=10)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 58: // STATE 36 - Tp3.pml:138 - [opDataIn!1] (0:0:0 - 1)
		IfNotBlocked
		reached[1][36] = 1;
		if (q_full(now.opDataIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opDataIn);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opDataIn, 0, 1, 1);
		_m = 2; goto P999; /* 0 */
	case 59: // STATE 38 - Tp3.pml:139 - [opOut!2] (0:0:0 - 1)
		IfNotBlocked
		reached[1][38] = 1;
		if (q_full(now.opOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opOut);
		sprintf(simtmp, "%d", 2); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opOut, 0, 2, 1);
		_m = 2; goto P999; /* 0 */
	case 60: // STATE 41 - Tp3.pml:141 - [opDataOut??6] (0:0:1 - 1)
		reached[1][41] = 1;
		if (q_len(now.opDataOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opDataOut, 1, 6))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opDataOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opDataOut);
		sprintf(simtmp, "%d", 6); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 61: // STATE 42 - Tp3.pml:141 - [opOut!6] (0:0:0 - 1)
		IfNotBlocked
		reached[1][42] = 1;
		if (q_full(now.opOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opOut);
		sprintf(simtmp, "%d", 6); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opOut, 0, 6, 1);
		_m = 2; goto P999; /* 0 */
	case 62: // STATE 43 - Tp3.pml:142 - [opDataOut??4] (0:0:1 - 1)
		reached[1][43] = 1;
		if (q_len(now.opDataOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opDataOut, 1, 4))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opDataOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opDataOut);
		sprintf(simtmp, "%d", 4); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 63: // STATE 44 - Tp3.pml:142 - [opOut!4] (0:0:0 - 1)
		IfNotBlocked
		reached[1][44] = 1;
		if (q_full(now.opOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opOut);
		sprintf(simtmp, "%d", 4); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opOut, 0, 4, 1);
		_m = 2; goto P999; /* 0 */
	case 64: // STATE 45 - Tp3.pml:143 - [opDataOut??1] (0:0:1 - 1)
		reached[1][45] = 1;
		if (q_len(now.opDataOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opDataOut, 1, 1))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opDataOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opDataOut);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 65: // STATE 46 - Tp3.pml:143 - [opOut!1] (0:0:0 - 1)
		IfNotBlocked
		reached[1][46] = 1;
		if (q_full(now.opOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opOut);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opOut, 0, 1, 1);
		_m = 2; goto P999; /* 0 */
	case 66: // STATE 47 - Tp3.pml:144 - [authIn??4] (0:0:1 - 1)
		reached[1][47] = 1;
		if (q_len(now.authIn) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authIn, 1, 4))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authIn, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authIn);
		sprintf(simtmp, "%d", 4); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 67: // STATE 48 - Tp3.pml:144 - [authOut!3] (0:0:0 - 1)
		IfNotBlocked
		reached[1][48] = 1;
		if (q_full(now.authOut))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authOut);
		sprintf(simtmp, "%d", 3); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authOut, 0, 3, 1);
		_m = 2; goto P999; /* 0 */
	case 68: // STATE 56 - Tp3.pml:148 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[1][56] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC Client */
	case 69: // STATE 1 - Tp3.pml:31 - [authIn!4] (0:0:0 - 1)
		IfNotBlocked
		reached[0][1] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 4); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 4, 1);
		_m = 2; goto P999; /* 0 */
	case 70: // STATE 2 - Tp3.pml:32 - [printf('System \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][2] = 1;
		Printf("System \n");
		_m = 3; goto P999; /* 0 */
	case 71: // STATE 3 - Tp3.pml:35 - [authOut??3] (0:0:1 - 1)
		reached[0][3] = 1;
		if (q_len(now.authOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authOut, 1, 3))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authOut);
		sprintf(simtmp, "%d", 3); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 72: // STATE 4 - Tp3.pml:37 - [clientConnected = 0] (0:0:1 - 5)
		IfNotBlocked
		reached[0][4] = 1;
		(trpt+1)->bup.oval = ((int)now.clientConnected);
		now.clientConnected = 0;
#ifdef VAR_RANGES
		logval("clientConnected", ((int)now.clientConnected));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 73: // STATE 5 - Tp3.pml:38 - [count = 0] (0:0:1 - 1)
		IfNotBlocked
		reached[0][5] = 1;
		(trpt+1)->bup.oval = now.count;
		now.count = 0;
#ifdef VAR_RANGES
		logval("count", now.count);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 74: // STATE 6 - Tp3.pml:39 - [canBuyMeal = 0] (0:0:1 - 1)
		IfNotBlocked
		reached[0][6] = 1;
		(trpt+1)->bup.oval = ((int)now.canBuyMeal);
		now.canBuyMeal = 0;
#ifdef VAR_RANGES
		logval("canBuyMeal", ((int)now.canBuyMeal));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 75: // STATE 7 - Tp3.pml:40 - [canAddFunds = 0] (0:0:1 - 1)
		IfNotBlocked
		reached[0][7] = 1;
		(trpt+1)->bup.oval = ((int)now.canAddFunds);
		now.canAddFunds = 0;
#ifdef VAR_RANGES
		logval("canAddFunds", ((int)now.canAddFunds));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 76: // STATE 8 - Tp3.pml:41 - [accountLocked = 0] (0:0:1 - 1)
		IfNotBlocked
		reached[0][8] = 1;
		(trpt+1)->bup.oval = ((int)now.accountLocked);
		now.accountLocked = 0;
#ifdef VAR_RANGES
		logval("accountLocked", ((int)now.accountLocked));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 77: // STATE 9 - Tp3.pml:42 - [printf('LoogedOut \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][9] = 1;
		Printf("LoogedOut \n");
		_m = 3; goto P999; /* 0 */
	case 78: // STATE 10 - Tp3.pml:44 - [authIn!9] (0:0:0 - 1)
		IfNotBlocked
		reached[0][10] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 9); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 9, 1);
		_m = 2; goto P999; /* 0 */
	case 79: // STATE 11 - Tp3.pml:45 - [authIn!8] (0:0:0 - 1)
		IfNotBlocked
		reached[0][11] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 8); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 8, 1);
		_m = 2; goto P999; /* 0 */
	case 80: // STATE 14 - Tp3.pml:48 - [authOut??9] (0:0:1 - 1)
		reached[0][14] = 1;
		if (q_len(now.authOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authOut, 1, 9))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authOut);
		sprintf(simtmp, "%d", 9); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 81: // STATE 15 - Tp3.pml:49 - [printf('WrongUsername \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][15] = 1;
		Printf("WrongUsername \n");
		_m = 3; goto P999; /* 0 */
	case 82: // STATE 16 - Tp3.pml:51 - [authIn!9] (0:0:0 - 1)
		IfNotBlocked
		reached[0][16] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 9); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 9, 1);
		_m = 2; goto P999; /* 0 */
	case 83: // STATE 17 - Tp3.pml:52 - [authIn!8] (0:0:0 - 1)
		IfNotBlocked
		reached[0][17] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 8); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 8, 1);
		_m = 2; goto P999; /* 0 */
	case 84: // STATE 20 - Tp3.pml:54 - [authOut??8] (0:0:1 - 1)
		reached[0][20] = 1;
		if (q_len(now.authOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authOut, 1, 8))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authOut);
		sprintf(simtmp, "%d", 8); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 85: // STATE 21 - Tp3.pml:55 - [printf('ValidUsername \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][21] = 1;
		Printf("ValidUsername \n");
		_m = 3; goto P999; /* 0 */
	case 86: // STATE 22 - Tp3.pml:57 - [authIn!7] (0:0:0 - 1)
		IfNotBlocked
		reached[0][22] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 7); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 7, 1);
		_m = 2; goto P999; /* 0 */
	case 87: // STATE 23 - Tp3.pml:58 - [authIn!6] (0:0:0 - 1)
		IfNotBlocked
		reached[0][23] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 6); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 6, 1);
		_m = 2; goto P999; /* 0 */
	case 88: // STATE 26 - Tp3.pml:61 - [authOut??7] (0:0:1 - 1)
		reached[0][26] = 1;
		if (q_len(now.authOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authOut, 1, 7))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authOut);
		sprintf(simtmp, "%d", 7); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 89: // STATE 27 - Tp3.pml:62 - [printf('WrongPassword \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][27] = 1;
		Printf("WrongPassword \n");
		_m = 3; goto P999; /* 0 */
	case 90: // STATE 28 - Tp3.pml:64 - [authIn!7] (0:0:0 - 1)
		IfNotBlocked
		reached[0][28] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 7); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 7, 1);
		_m = 2; goto P999; /* 0 */
	case 91: // STATE 29 - Tp3.pml:65 - [authIn!6] (0:0:0 - 1)
		IfNotBlocked
		reached[0][29] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 6); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 6, 1);
		_m = 2; goto P999; /* 0 */
	case 92: // STATE 32 - Tp3.pml:67 - [authOut??5] (0:0:1 - 1)
		reached[0][32] = 1;
		if (q_len(now.authOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authOut, 1, 5))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authOut);
		sprintf(simtmp, "%d", 5); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 93: // STATE 33 - Tp3.pml:68 - [printf('Account Blocked \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][33] = 1;
		Printf("Account Blocked \n");
		_m = 3; goto P999; /* 0 */
	case 94: // STATE 35 - Tp3.pml:70 - [authOut??6] (0:0:1 - 1)
		reached[0][35] = 1;
		if (q_len(now.authOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authOut, 1, 6))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authOut);
		sprintf(simtmp, "%d", 6); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 95: // STATE 36 - Tp3.pml:70 - [authIn!2] (0:0:0 - 1)
		IfNotBlocked
		reached[0][36] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 2); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 2, 1);
		_m = 2; goto P999; /* 0 */
	case 96: // STATE 37 - Tp3.pml:71 - [printf('ValidPassword \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][37] = 1;
		Printf("ValidPassword \n");
		_m = 3; goto P999; /* 0 */
	case 97: // STATE 46 - Tp3.pml:77 - [authOut??1] (0:0:1 - 1)
		reached[0][46] = 1;
		if (q_len(now.authOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authOut, 1, 1))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authOut);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 98: // STATE 47 - Tp3.pml:78 - [clientConnected = 1] (0:0:1 - 1)
		IfNotBlocked
		reached[0][47] = 1;
		(trpt+1)->bup.oval = ((int)now.clientConnected);
		now.clientConnected = 1;
#ifdef VAR_RANGES
		logval("clientConnected", ((int)now.clientConnected));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 99: // STATE 48 - Tp3.pml:79 - [fundsWithdrew = 0] (0:0:1 - 1)
		IfNotBlocked
		reached[0][48] = 1;
		(trpt+1)->bup.oval = ((int)now.fundsWithdrew);
		now.fundsWithdrew = 0;
#ifdef VAR_RANGES
		logval("fundsWithdrew", ((int)now.fundsWithdrew));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 100: // STATE 49 - Tp3.pml:80 - [canBuyMeal = 1] (0:0:1 - 1)
		IfNotBlocked
		reached[0][49] = 1;
		(trpt+1)->bup.oval = ((int)now.canBuyMeal);
		now.canBuyMeal = 1;
#ifdef VAR_RANGES
		logval("canBuyMeal", ((int)now.canBuyMeal));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 101: // STATE 50 - Tp3.pml:81 - [canAddFunds = 1] (0:0:1 - 1)
		IfNotBlocked
		reached[0][50] = 1;
		(trpt+1)->bup.oval = ((int)now.canAddFunds);
		now.canAddFunds = 1;
#ifdef VAR_RANGES
		logval("canAddFunds", ((int)now.canAddFunds));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 102: // STATE 51 - Tp3.pml:84 - [opIn!7] (0:0:0 - 1)
		IfNotBlocked
		reached[0][51] = 1;
		if (q_full(now.opIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opIn);
		sprintf(simtmp, "%d", 7); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opIn, 0, 7, 1);
		_m = 2; goto P999; /* 0 */
	case 103: // STATE 52 - Tp3.pml:85 - [opIn!5] (0:0:0 - 1)
		IfNotBlocked
		reached[0][52] = 1;
		if (q_full(now.opIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opIn);
		sprintf(simtmp, "%d", 5); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opIn, 0, 5, 1);
		_m = 2; goto P999; /* 0 */
	case 104: // STATE 53 - Tp3.pml:86 - [opIn!1] (0:0:0 - 1)
		IfNotBlocked
		reached[0][53] = 1;
		if (q_full(now.opIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.opIn);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.opIn, 0, 1, 1);
		_m = 2; goto P999; /* 0 */
	case 105: // STATE 54 - Tp3.pml:87 - [authIn!4] (0:0:0 - 1)
		IfNotBlocked
		reached[0][54] = 1;
		if (q_full(now.authIn))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.authIn);
		sprintf(simtmp, "%d", 4); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.authIn, 0, 4, 1);
		_m = 2; goto P999; /* 0 */
	case 106: // STATE 57 - Tp3.pml:91 - [opOut??6] (0:0:1 - 1)
		reached[0][57] = 1;
		if (q_len(now.opOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opOut, 1, 6))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opOut);
		sprintf(simtmp, "%d", 6); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 107: // STATE 58 - Tp3.pml:91 - [printf('FundsAdded \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][58] = 1;
		Printf("FundsAdded \n");
		_m = 3; goto P999; /* 0 */
	case 108: // STATE 60 - Tp3.pml:93 - [opOut??4] (0:0:1 - 1)
		reached[0][60] = 1;
		if (q_len(now.opOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opOut, 1, 4))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opOut);
		sprintf(simtmp, "%d", 4); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 109: // STATE 61 - Tp3.pml:93 - [printf('FundsWithdrew \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][61] = 1;
		Printf("FundsWithdrew \n");
		_m = 3; goto P999; /* 0 */
	case 110: // STATE 62 - Tp3.pml:94 - [fundsWithdrew = 1] (0:0:1 - 1)
		IfNotBlocked
		reached[0][62] = 1;
		(trpt+1)->bup.oval = ((int)now.fundsWithdrew);
		now.fundsWithdrew = 1;
#ifdef VAR_RANGES
		logval("fundsWithdrew", ((int)now.fundsWithdrew));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 111: // STATE 64 - Tp3.pml:96 - [opOut??1] (0:0:1 - 1)
		reached[0][64] = 1;
		if (q_len(now.opOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opOut, 1, 1))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opOut);
		sprintf(simtmp, "%d", 1); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 112: // STATE 65 - Tp3.pml:96 - [printf('BoughtMeal \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][65] = 1;
		Printf("BoughtMeal \n");
		_m = 3; goto P999; /* 0 */
	case 113: // STATE 67 - Tp3.pml:98 - [opOut??3] (0:0:1 - 1)
		reached[0][67] = 1;
		if (q_len(now.opOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opOut, 1, 3))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opOut);
		sprintf(simtmp, "%d", 3); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 114: // STATE 68 - Tp3.pml:98 - [printf('NoMoreFunds \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][68] = 1;
		Printf("NoMoreFunds \n");
		_m = 3; goto P999; /* 0 */
	case 115: // STATE 70 - Tp3.pml:100 - [opOut??2] (0:0:1 - 1)
		reached[0][70] = 1;
		if (q_len(now.opOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.opOut, 1, 2))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.opOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.opOut);
		sprintf(simtmp, "%d", 2); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 116: // STATE 71 - Tp3.pml:100 - [printf('NotEnoughFunds \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][71] = 1;
		Printf("NotEnoughFunds \n");
		_m = 3; goto P999; /* 0 */
	case 117: // STATE 73 - Tp3.pml:102 - [authOut??3] (0:0:1 - 1)
		reached[0][73] = 1;
		if (q_len(now.authOut) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.authOut, 1, 3))) continue;
		(trpt+1)->bup.oval = XX;
		;
		qrecv(now.authOut, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.authOut);
		sprintf(simtmp, "%d", 3); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 118: // STATE 81 - Tp3.pml:105 - [printf('End System \\n')] (0:0:0 - 1)
		IfNotBlocked
		reached[0][81] = 1;
		Printf("End System \n");
		_m = 3; goto P999; /* 0 */
	case 119: // STATE 82 - Tp3.pml:106 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[0][82] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */
	case  _T5:	/* np_ */
		if (!((!(trpt->o_pm&4) && !(trpt->tau&128))))
			continue;
		/* else fall through */
	case  _T2:	/* true */
		_m = 3; goto P999;
#undef rand
	}

