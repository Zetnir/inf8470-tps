#define N 4
mtype = {
    AskMenu,
    OrderFood,
    PayFood,
    AskDelivery,
    CheckedMenu,
    PreparingOrder,
    FoodPaid,
    ChooseDeliveryOrder
}
// -----------------------------------------------------------

// // Dispatcher -> Services
// chan init_menu = [4] of {mtype};
// chan init_orderFood = [4] of {mtype};
// chan init_schoolCreditCardAccount = [4] of {mtype};
// chan init_delivery = [4] of {mtype};

// // Services -> Dispatcher
// chan menu_init = [4] of {mtype};
// chan orderFood_init = [4] of {mtype};
// chan schoolCreditCardAccount_init = [4] of {mtype};
// chan delivery_init = [4] of {mtype};

// // Services -> Services
// chan delivery_orderFood = [8] of {mtype};
// chan orderFood_delivery = [8] of {mtype};
// chan orderFood_schoolCreditCardAccount = [8] of {mtype};
// chan schoolCreditCardAccount_orderFood = [8] of {mtype};

// -----------------------------------------------------------
// Menu Service
proctype MenuService(chan init_menu, menu_init ) {
    if
        :: init_menu?AskMenu -> printf("Vous consultez le menu...\n");
            menu_init!CheckedMenu;
    fi;
}

// Order Food Service
proctype OrderFoodService( chan init_orderFood,orderFood_init, schoolCreditCardAccount_orderFood, orderFood_schoolCreditCardAccount, delivery_orderFood ,orderFood_delivery) {
    if
        ::init_orderFood?OrderFood -> printf("Vous commandez un plat à récupérer sur place...\n");
            orderFood_schoolCreditCardAccount!PayFood;
            if
                ::schoolCreditCardAccount_orderFood?FoodPaid -> printf("Le plat est payé!\n");
            fi
            printf("La préparation du plat est terminé.\n");
            orderFood_init!PreparingOrder;

        ::delivery_orderFood?OrderFood -> printf("Vous commandez un plat à faire livrer...\n");
            orderFood_schoolCreditCardAccount!PayFood;
            if
                ::schoolCreditCardAccount_orderFood?FoodPaid -> printf("Le plat est payé!\n");
            fi
            printf("La préparation du plat est terminé.\n");
            orderFood_delivery!PreparingOrder;

    fi;
}

// School Credit Card Account Service
proctype SchoolCreditCardAccountService(chan init_schoolCreditCardAccount,schoolCreditCardAccount_init, orderFood_schoolCreditCardAccount, delivery_orderFood, schoolCreditCardAccount_orderFood ) {
    if
        ::init_schoolCreditCardAccount?PayFood -> printf("Vous payez avec votre carte de crédit...\n");
            schoolCreditCardAccount_init!FoodPaid;

        ::orderFood_schoolCreditCardAccount?PayFood -> 
            if
                ::delivery_orderFood?OrderFood -> printf("Vous payez avec votre carte de crédit un plat qui sera livré...\n");
                    schoolCreditCardAccount_orderFood!FoodPaid;
                // ::delivery_orderFood?OrderFood-> printf("Vous payez avec votre carte de crédit un plat qui ne sera pas livré...\n");
                //     schoolCreditCardAccount_orderFood!FoodPaid;
            fi;
    fi;
}

// Delivery Service
proctype DeliveryService(chan init_delivery,delivery_init, delivery_orderFood,orderFood_delivery ) {
    if
        ::init_delivery?AskDelivery -> printf("Vous demandez la livraison d'une commande...\n");
            delivery_orderFood!OrderFood;
    fi;
    if
        ::orderFood_delivery?PreparingOrder -> printf("La commande est prête à la livraison!\n");
            delivery_init!ChooseDeliveryOrder;
    fi;
}

// -----------------------------------------------------------
// Dispatcher
init {
    chan init_menu = [N] of {mtype};
    chan init_orderFood = [N] of {mtype};
    chan init_schoolCreditCardAccount = [N] of {mtype};
    chan init_delivery = [N] of {mtype};
    // Services -> Dispatcher
    chan menu_init = [N] of {mtype};
    chan orderFood_init = [N] of {mtype};
    chan schoolCreditCardAccount_init = [N] of {mtype};
    chan delivery_init = [N] of {mtype};

    // Services -> Services
    chan delivery_orderFood = [N] of {mtype};
    chan orderFood_delivery = [N] of {mtype};
    chan orderFood_schoolCreditCardAccount = [N] of {mtype};
    chan schoolCreditCardAccount_orderFood = [N] of {mtype};

    atomic {
        run MenuService(init_menu,menu_init);
        run OrderFoodService(init_orderFood,orderFood_init, schoolCreditCardAccount_orderFood, orderFood_schoolCreditCardAccount, delivery_orderFood ,orderFood_delivery);
        run SchoolCreditCardAccountService(init_schoolCreditCardAccount,schoolCreditCardAccount_init, orderFood_schoolCreditCardAccount, delivery_orderFood, schoolCreditCardAccount_orderFood );
        run DeliveryService(init_delivery,delivery_init, delivery_orderFood,orderFood_delivery);
    }
    do
        :: init_menu!AskMenu;
        :: init_orderFood!OrderFood;
        :: init_delivery!AskDelivery;
        
    od
    //init_menu!AskMenu;
    //init_orderFood!OrderFood;
    //init_schoolCreditCardAccount!PayFood;
    //init_delivery!AskDelivery;
    if 
        ::schoolCreditCardAccount_init?FoodPaid -> printf("La plat est payé!\n");
        ::menu_init?CheckedMenu -> printf("Le menu a été consulté!\n");
        ::orderFood_init?PreparingOrder -> printf("Le plat est prêt à être récupéré!\n");
        ::delivery_init?ChooseDeliveryOrder -> printf("La livraison est en cours!\n");
    fi
}