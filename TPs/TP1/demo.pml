mtype = {
    Ok
}

chan init_p1 = [5] of {mtype};

proctype P1(){
    if
        :: init_p1?Ok -> printf("allo2 \n");
    fi
}

init {
    run P1();

    init_p1!Ok;

    printf("allo \n");
}