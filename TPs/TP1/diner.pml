int d1 = 1;
int d2 = 0;
int tour = 1;

proctype P1() {
    do
        :: true -> d1 = 1;
            tour = 1;
            printf("Personne 1 en attente du couteau... \n");

            do
                :: (d2 == 0 || tour == 0) -> printf("Personne 1 utilise le couteau \n");
                    break;
            od

            d1 = 0;
    od
}

proctype P2() {
    do
        :: true -> d2 = 1;
            tour = 0;
            printf("Personne 2 en attente du couteau... \n");

            do
                :: (d1 == 0 || tour == 1) -> printf("Personne 2 utilise le couteau \n");
                    break;
            od

            d2 = 0;
    od
}

init {
    printf("Debut du diner \n");
    
    run P1();
    run P2();

    printf("Fin du diner \n");
}