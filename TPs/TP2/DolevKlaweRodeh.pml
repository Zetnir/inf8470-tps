#define N   6 /* nombre de processus */
#define I   3 /* processus avec le plus faible ident */
#define L   10 /* dimension du buffer (>= 2*N) */

chan q[N] = [L] of {byte}; /* N canaux de capacité L chacun */

byte nbre_leaders = 0;

proctype processus(chan in, out; byte ident){
    byte d, e, f;

    printf("MSC: %d\n", ident);

eligible:
    d = ident;
    do :: true -> out!d;
            in?e;
            if  :: (e == d) ->
                    printf("MSC: %d is LEADER\n", d);
                    nbre_leaders = nbre_leaders + 1;
                    goto stop /* d est le leader */
                :: else ->
                    skip
            fi;
            out!e;
            in?f;
            if  :: (e >= d) && (e >= f) ->
                    d = e;
                :: else ->
                    goto relai
            fi;
    od;

relai:
end:
    do  :: in?d -> 
            out!d;
    od;

stop:
    skip;
            
}

init{
    byte i;
    atomic {
        i = 1;
        printf("Nombre processus %d\n", N)
        printf("id processus %d\n", i);
        do  
            :: (i <= N) -> 
                run processus(q[i-1], q[i%N], (N-I-i)%N+1);
                i = i + 1
        od;
    }
}